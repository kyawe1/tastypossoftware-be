﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MUncle.Api.ViewModels;
using MUncle.Persistance.Context;
using MUncle.Persistance.Tables;
using Microsoft.EntityFrameworkCore;
using MUncle.Persistance.DTOS;
// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MUncle.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class OrderController : Controller
    {
        private readonly MUncleContext _context;
        public OrderController(MUncleContext context)
        {
            _context = context;
        }
        // GET: api/values
        [HttpGet]
        public IActionResult Get(int page=1,int perPage=20)
        {
            int skip = ((page < 0 ? 1 : page) - 1) * perPage;
            IEnumerable<OrderListViewModel> orders=_context.orders.Where(p => p.isDeleted == false).Skip(page).Take(perPage).Include(p=> p.Customer).Select(p =>
                new OrderListViewModel
                {
                    CustomerId=p.CustomerId.ToString(),
                    CustomerName=(p.Customer==null)? "" : p.Customer.Name,
                    DeliveryTime=p.DeliveryTime,
                    TotalPrice=p.TotalPrice,
                    TotalItems=p.TotalItems,
                    IsFinished=p.IsFinished,
                    humanreadableId=p.humanreadableId
                }
            ).ToList();
            int count = _context.orders.Where(p => p.isDeleted == false).Count();
            PaginateDTO<OrderListViewModel> data = new PaginateDTO<OrderListViewModel>()
            {
                total = count,
                data = orders,
                PageNumber = page,
                PerPage = perPage,
                TotalPage = (int)((count % (long)perPage == 0) ? count / (long)perPage : (count / (long)perPage) + 1)
            };
            return Ok(new { status = 200, data = data });
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public IActionResult Get(string id)
        {
            OrderViewModel? data = _context.orders.Where(p => p.isDeleted == false && p.Id == Guid.Parse(id)).Include(p => p.Customer).Include(p => p.OrderDetails).ThenInclude(q => q.Product).Select(p =>
                new OrderViewModel
                {
                    CustomerId = p.CustomerId.ToString(),
                    Customer = p.Customer,
                    DeliveryTime = p.DeliveryTime,
                    TotalPrice = p.TotalPrice,
                    TotalItems = p.TotalItems,
                    IsFinished = p.IsFinished,
                    humanreadableId = p.humanreadableId,
                    orderDetails = p.OrderDetails.Select(q => new OrderDetailViewModel
                    {
                        Price=q.Price,
                        NumberOfItems=q.NumberOfItems,
                        Product=new ProductViewModel
                        {
                            ProductName=q.Product.Name,
                            Description=q.Product.Description,
                            MeatType=q.Product.MeatType,
                            Picture=q.Product.Picture,
                            Id=q.ProductId.ToString(),
                            Price=Convert.ToInt32(q.Product.Price)
                        }
                    })

                }
            ).FirstOrDefault();
            if (data == null)
            {
                return NotFound(new { status = 404, message = "Not Found" });
            }
            return  Ok(new { status = 200, data = data }); ;
        }

        // POST api/values
        [HttpPost]
        public IActionResult Post([FromBody] OrderCreateViewModel value)
        {
            if (ModelState.IsValid)
            {
                Guid customerId;
                if(value.CustomerId == null)
                {
                    CustomerTable customer = new CustomerTable()
                    {
                        Address = value.CustomerAddress,
                        PhoneNumber = value.CustomerPhonenumber,
                        Name = value.CustomerName
                    };
                    customerId = customer.Id;
                }
                else
                {
                    customerId = Guid.Parse(value.CustomerId);
                }
                OrderTable order = new OrderTable
                {
                    CustomerId =customerId,
                    DeliveryTime = value.DeliveryTime.ToUniversalTime(),
                    TotalItems = value.TotalItems,
                    humanreadableId = (10000 + 1).ToString()
                };
                long total = 0;
                long totalItems = 0;
                IEnumerable<OrderDetailTable> details = new List<OrderDetailTable>();
                foreach (var i in value.orderDetails)
                {
                    OrderDetailTable orderDetail = new OrderDetailTable
                    {
                        ProductId = i.ProductId,
                        OrderId = order.Id,
                        NumberOfItems = i.NumberOfItems,
                        Price = i.Price
                    };
                    total += (i.Price * i.NumberOfItems);
                    totalItems += i.NumberOfItems;
                    details.Append(orderDetail);
                }
                order.TotalPrice = total;
                order.TotalItems = totalItems;
                _context.orders.Add(order);
                _context.orderDetails.AddRange(details);
                return Ok(new { status = 200, message = "Order Create Sucessfully!" });
            }
            return Ok(new { status = 400, message = "Order Create Not Sucessfully!" });
        }

        // PUT api/values/5
        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody] string value)
        //{
            //Todo: define the update function.
        //}

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public IActionResult Delete(string id)
        {
            try
            {
                OrderTable? order=_context.orders.Where(p => p.isDeleted == false && p.Id == Guid.Parse(id)).FirstOrDefault();
                if (order != null)
                {
                    IEnumerable<OrderDetailTable> orderDetails=_context.orderDetails.Where(p => p.isDeleted == false && p.OrderId == order.Id).ToList();
                    foreach(var i in orderDetails)
                    {
                        i.isDeleted = true;
                    }
                    order.isDeleted = true;
                    _context.orders.Update(order);
                    _context.orderDetails.UpdateRange(orderDetails);
                    return Ok(new { status = 200, message = "deleted successfully" });
                }
                return NotFound(new { status = 404, message = "Delete not successfully" });
            }catch(Exception e)
            {
                return Ok(new { status = 400, message = "Delete not successfully" });
            }
        }
    }
}

