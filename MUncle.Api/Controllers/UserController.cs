﻿using System;
using Microsoft.AspNetCore.Mvc;
using MUncle.Api.ViewModels;
using MUncle.Persistance.Context;
using MUncle.Persistance.DTOS;
using MUncle.Persistance.Tables;
using Microsoft.EntityFrameworkCore;

namespace MUncle.Api.Controllers
{

    [ApiController]
    [Route("api/[controller]/[action]")]
    public class UserController : ControllerBase
    {
        private readonly MUncleContext _context;
        [HttpGet]
        public IActionResult Index(int page = 1, int perPage = 20)
        {
            int skip = ((page < 0 ? 1 : page) - 1) * perPage;
            IEnumerable<UserViewModel> userList = _context.userTables.Where(p => p.isDeleted == false).Skip(skip).Take(perPage).AsNoTrackingWithIdentityResolution().Select(p => new UserViewModel
            {
                Name = p.Name,
                Email = p.Email,
                Address = p.Address,
                Birthdate = p.Birthdate.ToLocalTime(),
                Id = p.Id.ToString()
            }).ToList();
            long count = _context.userTables.AsNoTrackingWithIdentityResolution().Where(p => p.isDeleted == false).Count();
            PaginateDTO<UserViewModel> data = new PaginateDTO<UserViewModel>
            {
                data = userList,
                total = count,
                PerPage = perPage,
                PageNumber = page,
                TotalPage = (int)((count % perPage == 0) ? count / perPage : (count / perPage) + 1)
            };
            return Ok(new { status = 200, data = data });
        }
        [HttpPost("{Id}")]
        public IActionResult Detail(string Id)
        {
            UserViewModel? model = _context.userTables.Where(p => p.isDeleted == false && p.Id == Guid.Parse(Id)).Select(p => new UserViewModel
            {
                Name = p.Name,
                Password = p.Password,
                Address = p.Address,
                Birthdate = p.Birthdate.ToLocalTime(),
                Email = p.Email,
                Id = p.Id.ToString()
            }).FirstOrDefault();
            if (model == null)
            {
                return NotFound(new { status = 404, message = "User Not Found" });
            }
            return Ok(new { status = 200, data = model });
        }
        [HttpPost]
        public IActionResult Create(UserCreateViewModel model)
        {
            if (ModelState.IsValid)
            {
                UserTable user = new UserTable()
                {
                    Name=model.Name,
                    Email=model.Email,
                    Password=model.Password,
                    Address=model.Address,
                    Birthdate=model.Birthdate.ToUniversalTime()
                };
                _context.userTables.Add(user);
                _context.SaveChanges();
                return Ok(new { status="OK" , message="User Created Successfully" });
            }
            return Ok(new { status=404 , message = "User Created Not Successfully!" });
        }
        [HttpPut]
        public IActionResult Edit(UserViewModel model)
        {
            if (ModelState.IsValid)
            {
                UserTable? objects= _context.userTables.Where(p => p.isDeleted == false && p.Id == Guid.Parse(model.Id)).AsNoTrackingWithIdentityResolution().FirstOrDefault();
                if (objects != null)
                {
                    UserTable user = new UserTable()
                    {
                        Id = Guid.Parse(model.Id),
                        Name = model.Name,
                        Email = model.Email,
                        Password = model.Password,
                        Address = model.Address,
                        Birthdate = model.Birthdate.ToUniversalTime()
                    };
                    _context.userTables.Add(user);
                    _context.SaveChanges();
                    return Ok(new { status = "OK", message = "User Update Successfully" });
                }
            }
            return Ok(new { status = 404, message = "User Update Not Successfully!" });
        }
        [HttpDelete("{Id}")]
        public IActionResult Delete(string Id)
        {
            try
            {
                UserTable? user = _context.userTables.Where(p => p.Id == Guid.Parse(Id)).FirstOrDefault();
                if (user != null)
                {
                    user.isDeleted = true;
                    user.Password = "12345";
                    _context.userTables.Update(user);
                    _context.SaveChanges();
                    return Ok(new { status = 200, message = "Deleted user Successfully" });
                }
                return Ok(new { status = 404, message = "User Not Found!" });

            }
            catch (Exception e)
            {
                return Ok(new { status = 404, message = "User Not Found!" });

            }
        }
        public UserController(MUncleContext context)
        {
            _context = context;
        }
    }
}

