﻿using System;
using Microsoft.AspNetCore.Mvc;
using MUncle.Persistance.Context;
using MUncle.Api.ViewModels;
using MUncle.Persistance.DTOS;
using MUncle.Persistance.Tables;
using System.Net;
using System.Xml.Linq;

namespace MUncle.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class CustomerController : ControllerBase
    {
        private readonly MUncleContext _context;
        [HttpGet]
        public IActionResult Index(int page = 1, int perPage = 20)
        {
            int skip = ((page < 0 ? 1 : page) - 1) * perPage;

            IEnumerable<CustomerViewModel> customers = _context.customers.Where(p => p.isDeleted == false).Skip(skip).Take(perPage).Select(p => new CustomerViewModel
            {
                Id=p.Id.ToString(),
                Name = p.Name,
                Address = p.Address,
                PhoneNumber = p.PhoneNumber
            }).ToList();
            long count = _context.customers.Where(p => p.isDeleted == false).Count();
            PaginateDTO<CustomerViewModel> data = new PaginateDTO<CustomerViewModel>
            {
                data = customers,
                PerPage = perPage,
                total = count,
                PageNumber = page,
                TotalPage = (int)((count % perPage != 0) ? (count / perPage) + 1 : count / perPage)
            };
            return Ok(new { status = 200, data = data });
        }
        [HttpPost("{Id}")]
        public IActionResult Detail(string Id)
        {
            CustomerViewModel? model = _context.customers.Where(p => p.Id == Guid.Parse(Id)).Select(p => new CustomerViewModel
            {
                Name = p.Name,
                Address = p.Address,
                PhoneNumber = p.PhoneNumber
            }).FirstOrDefault();
            if (model == null)
            {
                return NotFound(new { status = 404, message = "Customer Not Found!" });
            }
            return Ok(new { status = 200, data = model });
        }
        [HttpPost]
        public IActionResult Autocomplete(CustomerCreateViewModel model)
        {

            return Ok(new { status = 200, data = "" });
        }
        [HttpPost]
        public IActionResult Create(CustomerCreateViewModel model)
        {
            if (ModelState.IsValid)
            {
                CustomerTable customer = new CustomerTable()
                {
                    Address = model.Address,
                    PhoneNumber = model.PhoneNumber,
                    Name = model.Name
                };
                _context.customers.Add(customer);
                _context.SaveChanges();
                return Ok(new { status = 200, message = "Customer Created Successfully !" });
            }
            return Ok(new { status = 400, message = "Customer Created Not Successfully !" });
        }
        [HttpPut]
        public IActionResult Edit(CustomerViewModel model)
        {
            if (ModelState.IsValid)
            {
                CustomerTable? cus = _context.customers.Where(p => p.Id == Guid.Parse(model.Id) && p.isDeleted == false).FirstOrDefault();
                if (cus != null)
                {
                    CustomerTable customer = new CustomerTable()
                    {
                        Id = Guid.Parse(model.Id),
                        Address = model.Address,
                        PhoneNumber = model.PhoneNumber,
                        Name = model.Name
                    };
                    _context.customers.Update(customer);
                    _context.SaveChanges();
                    return Ok(new { status = 200, message = "Customer Updated Successfully !" });
                }
            }
            return Ok(new { status = 400, message = "Customer Updated Not Successfully !" });
        }
        [HttpDelete("{Id}")]
        public IActionResult Delete(string Id)
        {
            try
            {
                CustomerTable? customer = _context.customers.Where(p => p.isDeleted == false && p.Id == Guid.Parse(Id)).FirstOrDefault();
                if (customer != null)
                {
                    customer.isDeleted = true;
                    _context.customers.Update(customer);
                    _context.SaveChanges();
                    return Ok(new { status = 200, message = "Customer Deleted Successfully" });
                }
                return Ok(new { status = 400, messsage = "Customer Deleted not Successfully" });
            }
            catch (Exception e)
            {
                return Ok(new { status = 400, messsage = "Customer Deleted not Successfully" });
            }
        }
        public CustomerController(MUncleContext context)
        {
            _context = context;
        }
    }
}

