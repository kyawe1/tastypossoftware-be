﻿using System;
using MUncle.Api.ViewModels;
using MUncle.Persistance.DTOS;
using MUncle.Persistance.Context;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using MUncle.Persistance.Tables;
using Microsoft.EntityFrameworkCore;
using MUncle.Api.Services;

namespace MUncle.Api.Controllers
{
	[ApiController]
	[Route("api/[controller]/[action]")]
	public class ProductController:ControllerBase
	{
		private readonly MUncleContext _context;
		public ProductController(MUncleContext context)
		{
			_context = context;
		}
		[HttpGet]
		public IActionResult Index([FromQuery]int page=1,[FromQuery]int perPage=20)
		{
			int skip = 0;
			if(page!=0)
			{
				skip = (page - 1)*perPage;
			}
			IEnumerable<ProductViewModel> products=_context.products.Skip(skip).Take(perPage).AsNoTrackingWithIdentityResolution().Where(p=> p.isDeleted==false).Take(10).Select(p=>new ProductViewModel
			{
				ProductName=p.Name,
				Id=p.Id.ToString(),
				Description=p.Description,
				Picture= FileService.getFileAsBase64String(p.Picture),
				Price=Convert.ToInt32(p.Price),
				MeatType=p.MeatType.ToString()
			}).ToList();
			long count = _context.products.AsNoTrackingWithIdentityResolution().Where(p=> p.isDeleted==false).Count();
			PaginateDTO<ProductViewModel> data = new PaginateDTO<ProductViewModel>()
			{
				total=count,
				data=products,
				PageNumber=page,
				PerPage=perPage,
				TotalPage=(int)((count%(long)perPage==0)? count/(long)perPage:(count/(long)perPage)+1)
			};
			return Ok(new { status = 200, data =data });
		}
		[HttpGet("{Id?}")]
		public IActionResult Detail(string? Id)
		{
			ProductViewModel? model=_context.products.AsNoTrackingWithIdentityResolution().Where(p => p.isDeleted == false && p.Id==Guid.Parse(Id)).Select(p=>new ProductViewModel
			{
				ProductName=p.Name,
				Id=p.Id.ToString(),
				Description=p.Description,
				Picture=FileService.getFileAsBase64String(p.Picture),
				Price=Convert.ToInt32(p.Price),
				MeatType=p.MeatType.ToString()
			}).FirstOrDefault();
			return Ok(new { status = 200, data = model });
		}
		[HttpPost]
		[ActionName("autocomplete")]
		public IActionResult Autocomplete([FromBody]string name)
		{
			IEnumerable<ProductListViewModel> model = _context.products.AsNoTrackingWithIdentityResolution().Where(p => p.isDeleted == false && p.Name.StartsWith(name)).Select(p => new ProductListViewModel
			{
				Name=p.Name,
				Id=p.Id.ToString()
			}).ToList();
			return Ok(new { status=200, data=model });
		}
		[HttpPost]
		public IActionResult Create(ProductCreateViewModel model)
		{
			if (ModelState.IsValid)
			{
				string path=FileService.saveLocalFile(model.Picture);
                ProductTable product = new ProductTable()
				{
					Name=model.ProductName,
					Description=model.Description,
					Picture=path,
					Price=model.Price.ToString(),
					MeatType=Guid.NewGuid().ToString()
				};
				_context.products.Add(product);
				_context.SaveChanges();
				return Ok(new { status=200, message="created Sucessfully!" });
			}
			return Ok(new { status=400 , message="Bad Request" });
		}
		[HttpPut]
		public IActionResult Edit(ProductViewModel model)
		{
            if(ModelState.IsValid)
            {
				ProductTable? productT=_context.products.AsNoTrackingWithIdentityResolution().Where(p => p.isDeleted == false && p.Id == Guid.Parse(model.Id)).FirstOrDefault();
				if (productT != null)
				{
                    ProductTable product = new ProductTable()
                    {
                        Id = Guid.Parse(model.Id),
                        Name = model.ProductName,
                        Description = model.Description,
                        Picture = model.Picture,
                        Price = model.Price.ToString(),
                        MeatType = model.MeatType
                    };
                    _context.products.Update(product);
                    _context.SaveChanges();
                    return Ok(new { status = 200, message = "Edited Sucessfully!" });
                }
            }
            return Ok(new { status = 400, message = "Bad Request" });
        }
		[HttpDelete("{Id}")]
		public IActionResult Delete(string Id)
		{
			try
			{
                ProductTable? product = _context.products.Where(p => p.Id == Guid.Parse(Id)).FirstOrDefault();
                if (product != null)
                {
					product.isDeleted = true;
                    _context.products.Update(product);
                    _context.SaveChanges();
                    return Ok(new { status = 200, message = "Delete product Successfully!" });
                }
                return Ok(new { status = 400, message = "Product Not Found!" });
            }catch(Exception e)
			{
				return BadRequest(new { status = 400, message = "Route not Correct" });
			}
		}

	}
}

