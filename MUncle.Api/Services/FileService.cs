﻿using System;
using System.Drawing;
namespace MUncle.Api.Services
{
	public class FileService
	{
		/// <summary>
		/// 
		/// </summary>
		/// <exception>Exception</exception>
		/// <returns>string</returns>
		public static string saveLocalFile(string base64s)
		{
			byte[] bytes=Convert.FromBase64String(base64s);
			string filename = $"Img{DateTime.Now.ToString("YMdhmsf")}.png";
			using(MemoryStream m=new MemoryStream(bytes))
			{
				FileStream file = File.Create("/UploadFile/"+filename);
				m.CopyTo(file);
				file.Close();
			}
			return "/UploadFile/"+filename;
		}
		public static string getFileAsBase64String(string fileName)
		{
			byte[] bytes=File.ReadAllBytes(fileName);
			return Convert.ToBase64String(bytes);
		}
		public FileService()
		{
		}
	}
}

