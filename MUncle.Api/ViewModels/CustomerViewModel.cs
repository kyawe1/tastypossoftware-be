﻿using System;
namespace MUncle.Api.ViewModels
{
	public class CustomerViewModel
	{
		public string Id { set; get; }
		public string Name { set; get; }
		public string Address { set; get; }
		public string PhoneNumber { set; get; }
		public CustomerViewModel()
		{
		}
	}
    public class CustomerCreateViewModel
    {
        public string Name { set; get; }
        public string Address { set; get; }
        public string PhoneNumber { set; get; }
        public CustomerCreateViewModel()
        {
        }
    }
    public class CustomerAutocompleteViewModel
	{
		public string Id { set; get; }
		public string Name { set; get; }
	}
}

