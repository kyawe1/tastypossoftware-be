﻿using System;
using MUncle.Persistance.Tables;
using System.ComponentModel.DataAnnotations.Schema;

namespace MUncle.Api.ViewModels
{
	public class OrderDetailViewModel
	{
        public long Price { set; get; }
        public ProductViewModel Product { set; get; }
        public long NumberOfItems { set; get; }
        public OrderDetailViewModel()
		{
		}
	}
	public class OrderDetailCreateViewModel
	{
        public long Price { set; get; }
        public Guid ProductId { set; get; }
        public long NumberOfItems { set; get; }
    }
}

