﻿using System;
namespace MUncle.Api.ViewModels
{
    public class UserViewModel
    {
        public UserViewModel()
        {

        }
        public string Name { set; get; }
        public string Email { set; get; }
        public string Password { set; get; }
        public string Address { set; get; }
        public DateTime Birthdate { set; get; }
        public string Id { set; get; }
    }
    public class UserCreateViewModel
    {
        public UserCreateViewModel()
        {

        }
        public string Name { set; get; }
        public string Email { set; get; }
        public string Password { set; get; }
        public string Address { set; get; }
        public DateTime Birthdate { set; get; }
        public string Id { set; get; }
    }

}

