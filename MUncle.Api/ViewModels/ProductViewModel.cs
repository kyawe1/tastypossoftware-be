﻿using System;
namespace MUncle.Api.ViewModels
{
    public class ProductIndexViewModel
    {
        public string Id { set; get; }
        public string Description { set; get; }
        public string MeatType { set; get; }
        public string Name { set; get; }
        public string Picture { set; get; }
        public string Price { set; get; }
        public DateTime UpdatedAt { set; get; }
    }
    public class ProductViewModel
    {
        public string Id { set; get; }
        public string Description { set; get; }
        public string MeatType { set; get; }
        public string ProductName { set; get; }
        public string Picture { set; get; }
        public int Price { set; get; }
    }
    public class ProductCreateViewModel
    {
        public string Description { set; get; }
        public string MeatType { set; get; }
        public string ProductName { set; get; }
        public string Picture { set; get; }
        public int Price { set; get; }
    }
    public class ProductListViewModel
    {
        public string Id { set; get; }
        public string MeatType { set; get; }
        public string Name { set; get; }
        public long Price { set; get; }
    }
    public class ProductAutocompleteViewModelÏ
    {
        public string Name { set; get; }
        public string Id { set; get; }
    }
    public class ProductSearchViewModel
    {
        public string Name { set; get; }
        public string MeatType { set; get; }
        public string Price { set; get; }
    }
}

