﻿using System;
using MUncle.Persistance.Tables;
namespace MUncle.Api.ViewModels
{
	public class OrderViewModel
	{
        public string CustomerId { set; get; }
        public virtual CustomerTable? Customer { set; get; }
        public long TotalItems { set; get; }
        public DateTime DeliveryTime { set; get; }
        public bool IsFinished { set; get; } = false;
        public long TotalPrice { set; get; }
        public string humanreadableId { set; get; }
		public IEnumerable<OrderDetailViewModel> orderDetails { set; get; }
        public OrderViewModel()
		{
		}
	}
    public class OrderListViewModel
    {
        public string CustomerId { set; get; }
        public string CustomerName { set; get; }
        public long TotalItems { set; get; }
        public DateTime DeliveryTime { set; get; }
        public bool IsFinished { set; get; } = false;
        public long TotalPrice { set; get; }
        public string humanreadableId { set; get; }
        public OrderListViewModel()
        {
        }
    }
    public class OrderCreateViewModel
	{
		public string? CustomerId { set; get; }
		public string CustomerName { set; get; }
		public string CustomerAddress { set; get; }
		public string CustomerPhonenumber { set; get; }
		public long TotalItems { set; get; }
		public DateTime DeliveryTime { set; get; }
		public bool IsFinished { set; get; }
		public IEnumerable<OrderDetailCreateViewModel> orderDetails { set; get; }
	}
}

