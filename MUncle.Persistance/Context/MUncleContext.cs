﻿using System;
using Microsoft.EntityFrameworkCore;
using MUncle.Persistance.Tables;

namespace MUncle.Persistance.Context
{
	public class MUncleContext : DbContext
	{
		public MUncleContext(DbContextOptions<MUncleContext> context):base(context)
		{
		}
		public DbSet<CustomerTable> customers { set; get; }
		public DbSet<ProductTable> products { set; get; }
		public DbSet<UserTable> userTables { set; get; }
        public DbSet<OrderTable> orders { set; get; }
        public DbSet<OrderDetailTable> orderDetails { set; get; }

    }
}

