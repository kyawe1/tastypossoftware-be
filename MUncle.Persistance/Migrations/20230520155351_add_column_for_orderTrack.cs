﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MUncle.Persistance.Migrations
{
    public partial class add_column_for_orderTrack : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "humanreadableId",
                table: "orders",
                type: "text",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "humanreadableId",
                table: "orders");
        }
    }
}
