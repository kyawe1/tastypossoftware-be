﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MUncle.Persistance.Context;

namespace MUncle.Persistance
{
	public static class Injector
	{
		public static void AddPostgres(this IServiceCollection services , IConfiguration config)
		{
			services.AddDbContext<MUncleContext>(option=> option.UseNpgsql(config.GetConnectionString("local")));
		}
	}
}

