﻿using System;
namespace MUncle.Persistance.DTOS
{
	public class PaginateDTO<T>
	{
		public long total { set; get; }
		public IEnumerable<T> data { set; get; }
		public int PageNumber { set; get; }
		public int TotalPage { set; get; }
		public int PerPage { set; get; }
		public PaginateDTO()
		{
		}
	}
}

