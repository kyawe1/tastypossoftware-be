﻿using System;
namespace MUncle.Persistance.Tables
{
	public class CustomerTable:BaseTable
	{
		public string Name { set; get; }
		public string PhoneNumber { set; get; }
		public string Address { set; get; }
		public CustomerTable()
		{
		}
	}
}

