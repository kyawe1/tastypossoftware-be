﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace MUncle.Persistance.Tables
{
	public class OrderDetailTable:BaseTable
	{
		public long Price { set; get; }
		public Guid ProductId { set; get; }
		public long NumberOfItems { set; get; }
		[ForeignKey("ProductId")]
		public virtual ProductTable? Product{ set; get; }
		public Guid OrderId { set; get; }
		[ForeignKey("OrderId")]
		public virtual OrderTable? Order { set; get; }
		public OrderDetailTable()
		{
		}
	}
}

