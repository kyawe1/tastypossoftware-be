﻿using System;
namespace MUncle.Persistance.Tables
{
	public class BaseTable
	{
		public Guid Id { set; get; }
		public DateTime CreatedAt { set; get; }
		public DateTime UpdatedAt { set; get; }
		public bool isDeleted { set; get; }
		public BaseTable()
		{
			Id = Guid.NewGuid();
			CreatedAt = DateTime.UtcNow;
			UpdatedAt = DateTime.UtcNow;
			isDeleted = false;
		}
	}
}

