﻿using System;
namespace MUncle.Persistance.Tables
{
	public class ProductTable:BaseTable
	{
		public string Name { set; get; }
		public string Price { set; get; }
		public string Description { set; get; }
		public string MeatType { set; get; }
		public string Picture { set; get; }
		public ProductTable()
		{
		}
	}
}

