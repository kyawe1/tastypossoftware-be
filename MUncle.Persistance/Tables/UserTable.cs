﻿using System;
namespace MUncle.Persistance.Tables
{
	public class UserTable:BaseTable
	{
		public string Name { set; get; }
		public string Email { set; get; }
		public string Password { set; get; }
		public string Address { set; get; }
		public DateTime Birthdate { set; get; }
		public UserTable()
		{
		}
	}
}

