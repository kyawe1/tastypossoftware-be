﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace MUncle.Persistance.Tables
{
	public class OrderTable:BaseTable
	{
		public Guid CustomerId { set; get; }
		[ForeignKey("CustomerId")]
		public virtual CustomerTable? Customer { set; get; }
		public long TotalItems { set; get; }
		public DateTime DeliveryTime { set; get; }
		public bool IsFinished { set; get; } = false;
		public long TotalPrice { set; get; }
		public string humanreadableId { set; get; }
		public virtual IEnumerable<OrderDetailTable> OrderDetails { set; get; }
		public OrderTable()
		{
		}
	}
}

